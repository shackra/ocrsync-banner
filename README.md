# OCRSync #

---

Simple and reliable tool written in [python](http://oython.org), made to easily keep you in sync with the musical repository of [ocremix.org](http://ocremix.org).

**Now with graphical interface!**

![Captura de pantalla de 2014-07-27 02:20:47.png](https://bitbucket.org/repo/574Xzd/images/3729952660-Captura%20de%20pantalla%20de%202014-07-27%2002%3A20%3A47.png)

More info in the [Wiki](https://bitbucket.org/BreadMaker/ocrsync/wiki)